# REAL changelog

## Version [0.5](https://git.ecdf.ed.ac.uk/pguaglia/real/commits/0.5) (2021-09-28)

#### New features

- In-app help on internal commands. Issuing the command `.help` at REAL's prompt
  shows the list of available commands, along with their usage and description.
- Atomic selection conditions now support less-than comparisons using the newly
  introduced `<` operator. This compares strings lexicographically, but behaves
  differently on strings that represent integer values. More precisely, if the
  two strings being compared can be *both* parsed as integers, then they are
  compared as integers; otherwise, lexicographic comparison is used. As a side
  effect, we lose the ability to compare lexicographically strings that consist
  only of digits. This will be rectified once proper types are introduced.

#### Changed functionality

- Renaming is now defined as an operation that only accepts one replacement, in
  place of a list thereof. That possibility is retained as a syntactic shortcut,
  which is parsed as sequence of nested renamings. For example,
  `<R>[A->B,B->C](table)` is short for `<R>[B->C](<R>[A->B](table))`.

#### API improvements

- Get list of replacements in the renaming operation.
- Getters for conditions and terms.
- New abstract superclass for unary operations.

## Version [0.4](https://git.ecdf.ed.ac.uk/pguaglia/real/commits/0.4) (2020-09-08)

#### New features

- Adding tables via the interactive prompt. This can be done by issuing the
  following command:

  ```.add TABLE-NAME( COMMA-SEPARATED-LIST-OF-ATTRIBUTES ) : PATH-TO-CSV-FILE```

- Database schema. By default, the application starts with an empty database
  schema that can be populated with
  * tables, using the newly introduced `.add` command;
  * views, using the existing syntax `VIEW-NAME := VIEW-DEFINITION`.

  The schema now records the correspondence between a table name and the CSV
  file containing its data, which is loaded into main memory only when needed to
  evaluate a query. Attribute names are not recorded in the CSV files, but in
  the schema.

- Persistent views. The schema can be serialized to JSON with the `.save
  [PATH-TO-JSON-FILE]` command for reuse across sessions. If invoked without
  specifying the path to a file, `.save` uses either
  * the file in which the previously loaded schema is stored (see below), or
  * the temporary file created when starting the application, if no schema was
    previously loaded.

  Note that the values of the configuration parameters (e.g., `.eval` and
  `.tree`) are not saved in the exported JSON file.

  A schema file in JSON format can be loaded using either:
  * the command-line switch `-d` (previously used to specify the CSV files to
    import), or
  * the command `.load PATH-TO-JSON-FILE` at the interactive prompt.

- The `.drop` command can now also delete tables as well as views.

#### Changed functionality

- The command-line switch `-d` now expects a JSON schema file rather than the
  path to a folder containing CSV files.
- The dependency on SQLite is no longer needed.

## Version [0.3](https://git.ecdf.ed.ac.uk/pguaglia/real/commits/0.3) (2020-04-08)

#### New features

- Command line options:
  * `-d`, `--data` to specify the data directory with the CSV files,
  * `-e`, `--eval` to control the query execution semantics (corresponds to the
    `.eval` command in interactive mode),
  * `-q`, `--query` to specify a query expression to be executed in a batch way,
  * `-t`, `--tree` to control the printing of syntax trees (corresponds to the
    `.tree` command in interactive mode).

- Views (virtual tables). In interactive mode, a view is created with the syntax
  `VIEW-NAME := EXPRESSION`, where `EXPRESSION` may mention previously defined
  views. A view can be deleted with the command `.drop VIEW-NAME`, which results
  in an error if the view to be deleted is still used (directly or indirectly)
  in the definition of another view. Views do not persist across runs of the
  application.

#### Bug fixes

- A blank input at the command prompt now does not cause an EmptyStackException
  to be thrown (issue #2).

## Version [0.2.1](https://git.ecdf.ed.ac.uk/pguaglia/real/commits/0.2.1) (2020-03-02)

#### Bug fixes

- The union operation now returns the correct answers under set semantics.

## Version [0.2](https://git.ecdf.ed.ac.uk/pguaglia/real/commits/0.2) (2020-02-27)

#### New features

- Evaluation under bag semantics. The evaluation of RA expressions (`.eval`) can
  now be set to `OFF`, `SET` (set semantics; default) or `BAG` (bag semantics).

- Added duplicate elimination. Under bag semantics, this RA operation removes
  duplicates (multiple occurrences of the same row) from a table.

## Version [0.1](https://git.ecdf.ed.ac.uk/pguaglia/real/commits/0.1) (2020-01-30)

#### New features

- Import database tables from the CSV files in a given directory. For each CSV
  file, a table with the same name is created; its column names (table header)
  are taken from the first row of values in the file.

- Evaluate RA expressions under set semantics. Query execution is controlled by
  the `.eval` switch, which can be set to `ON` (default) or `OFF` (expressions
  are validated against the database schema, but they are not executed).

- Display the syntax tree of RA expressions. This is controlled by the `.tree`
  option, which can be set to `ON` (show the syntax tree) or `OFF` (default).
