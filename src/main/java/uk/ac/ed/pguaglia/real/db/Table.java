package uk.ac.ed.pguaglia.real.db;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.lang3.ArrayUtils;

import de.vandermeer.asciitable.AsciiTable;
import de.vandermeer.asciitable.CWC_LongestLine;
import de.vandermeer.skb.interfaces.transformers.textformat.TextAlignment;
import uk.ac.ed.pguaglia.real.lang.Condition;

public class Table {

	private List<String[]> records;
	private List<String> attributes;

	private Table() {
		this.records = new ArrayList<>();
		this.attributes = new ArrayList<>();
	}

	public Table(List<String[]> records, List<String> attributes) {
		this();
		this.records.addAll(records);
		this.attributes.addAll(attributes);
	}

	public List<String> getAttributes() {
		return new ArrayList<>(attributes);
	}

	public List<String[]> getRecords() {
		return new ArrayList<>(records);
	}

	protected Table sort(Comparator<? super String[]> c) {
		Table out = new Table(records, attributes);
		out.records.sort(c);
		return out;
	}

	public Table distinct() {
		Table out = sort(new RowComparator());
		String[] curr = null;
		ListIterator<String[]> it = out.records.listIterator();
		RowComparator c = new RowComparator();
		while (it.hasNext()) {
			String[] r = it.next();
			if (curr == null) {
				curr = r;
				continue;
			}
			if (c.compare(r, curr) == 0) {
				it.remove();
			} else {
				curr = r;
			}
		}
		return out;
	}

	public Table select(Condition cond) {
		Map<String,Integer> attr = new HashMap<>();
		for (String a : attributes) {
			attr.put(a, attributes.indexOf(a));
		}
		Table out = new Table();
		out.attributes.addAll(attributes);
		for (String[] r : records) {
			if (cond.satisfied(r, attr)) {
				out.records.add(r);
			}
		}
		return out;
	}

	public Table project(Set<String> attr) {
		Table out = new Table();
		int len = attr.size();
		out.attributes.addAll(attr);
		for (String[] r : records) {
			String[] s = new String[len];
			int i = 0;
			for (String a : out.attributes) {
				int j = attributes.indexOf(a);
				s[i] = r[j];
				i++;
			}
			out.records.add(s);
		}
		return out;
	}

	public Table rename(Map<String,String> repl) {
		List<String> attr = getAttributes();
		for (String a : repl.keySet()) {
			int i = attributes.indexOf(a);
			attr.set(i, repl.get(a));
		}
		return new Table(this.records, attr);
	}

	public Table product(Table t) {
		return product(this,t);
	}

	public static Table product(Table t1, Table t2) {
		Table out = new Table();
		out.attributes.addAll(t1.attributes);
		out.attributes.addAll(t2.attributes);
		for (String[] r : t1.records) {
			for (String[] s : t2.records) {
				out.records.add(ArrayUtils.addAll(r,s));
			}
		}
		return out;
	}

	public Table unionAll(Table t) {
		return Table.unionAll(this, t);
	}

	public static Table unionAll(Table t1, Table t2) {
		Table out = new Table();
		out.attributes.addAll(t1.attributes);
		out.records.addAll(t1.records);
		for (String[] r : t2.records) {
			out.records.add(permute(r,t2.attributes,t1.attributes));
		}
		return out;
	}

	public Table exceptAll(Table t) {
		return Table.exceptAll(this, t);
	}

	private static String[] permute(String[] r, List<String> attr, List<String> perm) {
		String[] s = new String[r.length];
		int i = 0;
		for (String a : perm) {
			int j = attr.indexOf(a);
			s[i] = r[j];
			i++;
		}
		return s;
	}

	public static Table exceptAll(Table t1, Table t2) {
		RowComparator c = new RowComparator();
		List<String[]> rec1 = t1.getRecords();
		List<String[]> rec2 = t2.getRecords().stream().map(x -> permute(x,t2.attributes,t1.attributes)).collect(Collectors.toList());

		rec1.sort(c);
		rec2.sort(c);

		ListIterator<String[]> it1 = rec1.listIterator();
		ListIterator<String[]> it2 = rec2.listIterator();
		List<String[]> outRecords = new ArrayList<>();
		int cmp = 0;
		String[] curr1 = null;
		String[] curr2 = null;

		while (true) {
			if (cmp < 0) {
				if (it1.hasNext()) {					
					curr1 = it1.next();
				} else {
					break;
				}
			}
			if (cmp > 0) {
				if (it2.hasNext()) {					
					curr2 = it2.next();
				} else {
					break;
				}
			}
			if (cmp == 0) {
				if (it1.hasNext() && it2.hasNext()) {					
					curr1 = it1.next();
					curr2 = it2.next();
				} else {
					break;
				}
			}
			cmp = c.compare(curr1, curr2);
			if (cmp < 0) {
				outRecords.add(curr1);
			}
		}
		while (it1.hasNext()) { // add remaining elements of t1
			outRecords.add(it1.next());
		}
		return new Table(outRecords, t1.attributes);
	}

	public Table intersectAll(Table t) {
		return Table.intersectAll(this, t);
	}

	public static Table intersectAll(Table t1, Table t2) {
		RowComparator c = new RowComparator();
		List<String[]> rec1 = t1.getRecords();
		List<String[]> rec2 = t2.getRecords();
		rec1.sort(c);
		rec2.sort(c);

		ListIterator<String[]> it1 = rec1.listIterator();
		ListIterator<String[]> it2 = rec2.listIterator();
		List<String[]> outRecords = new ArrayList<>();
		int cmp = 0;
		String[] curr1 = null;
		String[] curr2 = null;

		while (true) {
			if (cmp < 0) {
				if (it1.hasNext()) {					
					curr1 = it1.next();
				} else {
					break;
				}
			}
			if (cmp > 0) {
				if (it2.hasNext()) {					
					curr2 = permute(it2.next(),t2.attributes,t1.attributes);
				} else {
					break;
				}
			}
			if (cmp == 0) {
				if (it1.hasNext() && it2.hasNext()) {					
					curr1 = it1.next();
					curr2 = permute(it2.next(),t2.attributes,t1.attributes);
				} else {
					break;
				}
			}
			cmp = c.compare(curr1, curr2);
			if (cmp == 0) {
				outRecords.add(curr1);
			}
		}
		return new Table(outRecords, t1.attributes);
	}

	public AsciiTable toAsciiTable() {
		AsciiTable atable = new AsciiTable();
		atable.addRule();
		atable.addRow(this.getAttributes());
		atable.addRule();
		for (String[] r : this.getRecords()) {
			atable.addRow(Arrays.asList(r));
		}
		atable.addRule();

		atable.setTextAlignment(TextAlignment.LEFT);
		atable.getRawContent().get(1).setTextAlignment(TextAlignment.CENTER);
		atable.setPaddingLeftRight(1);
		atable.getRenderer().setCWC(new CWC_LongestLine());

		return atable;
	}

	@Override
	public String toString() {
		return toAsciiTable().render();
	}
}
