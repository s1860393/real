package uk.ac.ed.pguaglia.real.lang;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class Equality extends Condition {

	private Term left;
	private Term right;

	public Equality ( Term left, Term right ) {
		super( Condition.Type.COMPARISON );
		this.left = left;
		this.right = right;
	}

	public Term getLeftTerm() {
		return left;
	}

	public Term getRightTerm() {
		return right;
	}

	@Override
	public String toString() {
		return String.format( "%s = %s",
				left.toString(),
				right.toString() );
	}

	@Override
	public Set<String> signature() {
		Set<String> sign = new HashSet<String>();
		if (left.isAttribute()) {
			sign.add(left.getValue());
		}
		if (right.isAttribute()) {
			sign.add(right.getValue());
		}
		return sign;
	}

	@Override
	public boolean satisfied(String[] record, Map<String, Integer> attr) {
		String v1 = left.getValue(record, attr);
		String v2 = right.getValue(record, attr);
		return v1.equals(v2);
	}
}
