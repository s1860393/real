package uk.ac.ed.pguaglia.real.parsing;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Stack;

import uk.ac.ed.pguaglia.real.lang.BaseExpression;
import uk.ac.ed.pguaglia.real.lang.Condition;
import uk.ac.ed.pguaglia.real.lang.Conjunction;
import uk.ac.ed.pguaglia.real.lang.Difference;
import uk.ac.ed.pguaglia.real.lang.Disjunction;
import uk.ac.ed.pguaglia.real.lang.Distinct;
import uk.ac.ed.pguaglia.real.lang.Equality;
import uk.ac.ed.pguaglia.real.lang.Expression;
import uk.ac.ed.pguaglia.real.lang.Intersection;
import uk.ac.ed.pguaglia.real.lang.LessThan;
import uk.ac.ed.pguaglia.real.lang.Negation;
import uk.ac.ed.pguaglia.real.lang.Product;
import uk.ac.ed.pguaglia.real.lang.Projection;
import uk.ac.ed.pguaglia.real.lang.Renaming;
import uk.ac.ed.pguaglia.real.lang.ReplacementException;
import uk.ac.ed.pguaglia.real.lang.Selection;
import uk.ac.ed.pguaglia.real.lang.Term;
import uk.ac.ed.pguaglia.real.lang.Union;

public class RealListener extends RABaseListener {

	private class MapEntry<K,V> {
		K key;
		V val;

		public MapEntry(K key, V val) {
			this.key = key;
			this.val = val;
		}
	}

	private Stack<Expression> exprStack = new Stack<Expression>();
	private List<String> attrList = null;
	private Stack<List<String>> attrListStack = new Stack<List<String>>();
	private Stack<Term> termStack = new Stack<Term>();
	private Stack<Condition> condStack = new Stack<Condition>();
	private Stack<String> attrStack = new Stack<String>();
	private List<MapEntry<String,String>> replMap = null;
	private Stack<List<MapEntry<String,String>>> replMapStack = new Stack<>();
	private Expression parsedExpression = null;

	public Expression parsedExpression() {
		if (exprStack.size() != 1) {
			// something went wrong
		}
		if (parsedExpression == null) {
			parsedExpression = exprStack.pop();
		}
		return parsedExpression;
	}

	@Override
	public void exitBaseRelation(RAParser.BaseRelationContext ctx) {
		exprStack.push(new BaseExpression(ctx.getText()));
	}

	@Override
	public void exitDistinct(RAParser.DistinctContext ctx) {
		Expression expr = exprStack.pop();
		exprStack.push(new Distinct(expr));
	}

	@Override
	public void exitProduct(RAParser.ProductContext ctx) {
		Expression right = exprStack.pop();
		Expression left = exprStack.pop();
		exprStack.push(new Product(left, right));
	}

	@Override
	public void exitIntersection(RAParser.IntersectionContext ctx) {
		Expression right = exprStack.pop();
		Expression left = exprStack.pop();
		exprStack.push(new Intersection(left, right));
	}

	@Override
	public void exitUnion(RAParser.UnionContext ctx) {
		Expression right = exprStack.pop();
		Expression left = exprStack.pop();
		exprStack.push(new Union(left, right));
	}

	@Override
	public void exitDifference(RAParser.DifferenceContext ctx) {
		Expression right = exprStack.pop();
		Expression left = exprStack.pop();
		exprStack.push(new Difference(left, right));
	}

	@Override
	public void enterAttributeList(RAParser.AttributeListContext ctx) {
		attrList = new ArrayList<String>();
	}

	@Override
	public void exitAttributeList(RAParser.AttributeListContext ctx) {
		attrListStack.add(attrList);
	}

	@Override
	public void enterReplacementList(RAParser.ReplacementListContext ctx) {
		replMap = new ArrayList<MapEntry<String,String>>();
	}

	@Override
	public void exitReplacementList(RAParser.ReplacementListContext ctx) {
		replMapStack.add(replMap);
	}

	@Override
	public void exitAttribute(RAParser.AttributeContext ctx) {
		if (ctx.getParent() instanceof RAParser.ReplacementContext) {
			attrStack.push(ctx.getText());
		} else if (ctx.getParent() instanceof RAParser.AttributeListContext) {
			attrList.add(ctx.getText());
		} else if (ctx.getParent() instanceof RAParser.TermContext) {
			termStack.add(new Term(ctx.getText(), false));	
		}
	}

	@Override
	public void exitConstant(RAParser.ConstantContext ctx) {
		String value = ctx.getText().replace("'", "");
		termStack.push(new Term(value, true));
	}

	@Override
	public void exitProjection(RAParser.ProjectionContext ctx) {
		Expression expr = exprStack.pop();
		List<String> attr = attrListStack.pop();
		exprStack.push(new Projection(attr, expr));
	}

	@Override
	public void exitEquality(RAParser.EqualityContext ctx) {
		Term right = termStack.pop();
		Term left = termStack.pop();
		condStack.push(new Equality(left, right));
	}

	@Override
	public void exitLessThan(RAParser.LessThanContext ctx) {
		Term right = termStack.pop();
		Term left = termStack.pop();
		condStack.push(new LessThan(left, right));
	}

	@Override
	public void exitConjunction(RAParser.ConjunctionContext ctx) {
		Condition right = condStack.pop();
		Condition left = condStack.pop();
		condStack.push(new Conjunction(left, right));
	}

	@Override
	public void exitDisjunction(RAParser.DisjunctionContext ctx) {
		Condition right = condStack.pop();
		Condition left = condStack.pop();
		condStack.push(new Disjunction(left, right));
	}

	@Override
	public void exitNegation(RAParser.NegationContext ctx) {
		Condition c = condStack.pop();
		condStack.push(new Negation(c));
	}

	@Override
	public void exitSelection(RAParser.SelectionContext ctx) {
		Condition cond = condStack.pop();
		Expression expr = exprStack.pop();
		exprStack.push(new Selection(cond, expr));
	}

	@Override
	public void exitReplacement(RAParser.ReplacementContext ctx) {
		String v = attrStack.pop();
		String k = attrStack.pop();
		replMap.add(new MapEntry<>(k,v));
	}

	@Override
	public void exitRenaming(RAParser.RenamingContext ctx) {
		var repl = replMapStack.pop();
		var expr = exprStack.pop();
		for (var entry : repl) {
			try {
				HashMap<String, String> map = new HashMap<>();
				map.put(entry.key, entry.val);
				expr = new Renaming(map, expr);
			} catch (ReplacementException e) {
				throw new RuntimeException(ctx.getText(), e);
			}
		}
		exprStack.push(expr);
	}
}
